package inc.minuth.mi_chart.ui.base;

public class BasePresenter<V> implements MvpPresenter<V> {
    private V view;
    @Override
    public void attatch(V view) {
        this.view = view;
    }

    @Override
    public void distatch() {

    }

    @Override
    public V getView() {
        return view;
    }
}
