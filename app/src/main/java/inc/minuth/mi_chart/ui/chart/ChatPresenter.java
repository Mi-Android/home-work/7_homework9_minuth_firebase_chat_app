package inc.minuth.mi_chart.ui.chart;
import javax.inject.Inject;

import inc.minuth.mi_chart.callback.DataListenter;
import inc.minuth.mi_chart.data.firebase.model.Chat;
import inc.minuth.mi_chart.data.firebase.repository.ChatRepository;
import inc.minuth.mi_chart.data.firebase.repository.UserRepository;
import inc.minuth.mi_chart.ui.base.BasePresenter;

public class ChatPresenter<V> extends BasePresenter<V>
{
    private ChatRepository chatRepository;
    private UserRepository userRepository;
    @Inject
    public ChatPresenter(ChatRepository chatRepository,UserRepository userRepository)
    {
        this.chatRepository = chatRepository;
        this.userRepository = userRepository;

    }
    public void sendChat(Chat chat)
    {
        chatRepository.sendMessageChat(chat);
    }
    public void addUserDataListenter(DataListenter<Long> listenter)
    {
        userRepository.addDataListenter(listenter);
    }



}
