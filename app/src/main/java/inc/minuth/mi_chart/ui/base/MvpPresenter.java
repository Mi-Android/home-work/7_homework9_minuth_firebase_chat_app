package inc.minuth.mi_chart.ui.base;

public interface MvpPresenter<V> {
    void attatch(V view);
    void distatch();
    V getView();
}
