package inc.minuth.mi_chart.ui.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.firebase.ui.auth.AuthUI;
import com.firebase.ui.auth.IdpResponse;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import inc.minuth.mi_chart.ChatApp;
import inc.minuth.mi_chart.R;
import inc.minuth.mi_chart.data.firebase.model.User;
import inc.minuth.mi_chart.data.firebase.repository.UserRepository;
import inc.minuth.mi_chart.di.component.AppComponent;
import inc.minuth.mi_chart.ui.chart.ChartFragment;

public class MainActivity extends AppCompatActivity implements MainView{

    private static final int RC_SIGN_IN = 1000;
    private TextView tvWelcome;
    private Button btnTryAgain;
    @Inject MainPresenter<MainView> presenter;

    List<AuthUI.IdpConfig> providers = Arrays.asList(
            new AuthUI.IdpConfig.EmailBuilder().build());
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ((ChatApp)getApplication()).getAppComponent().inject(this);
        presenter.attatch(this);
        presenter.checkUser();

    }
    @Override
    public void loadChart()
    {
        getSupportFragmentManager().beginTransaction()
                .add(R.id.layout_container,new ChartFragment())
                .commit();
    }
    @Override
    public void signUp()
    {
        startActivityForResult(
                AuthUI.getInstance().createSignInIntentBuilder().build(),
                RC_SIGN_IN);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RC_SIGN_IN) {
            //IdpResponse response = IdpResponse.fromResultIntent(data);

            if (resultCode == RESULT_OK) {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                presenter.addUser(new User(user.getUid(),user.getEmail(),user.getDisplayName()));
                loadChart();
            } else {

                // Sign in failed. If response is null the user canceled the
                // sign-in flow using the back button. Otherwise check
                // response.getError().getErrorCode() and handle the error.
                // ...
            }
        }
    }
}
