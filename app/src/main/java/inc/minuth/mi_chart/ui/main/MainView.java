package inc.minuth.mi_chart.ui.main;

public interface MainView
{
    void loadChart();
    void signUp();
}
