package inc.minuth.mi_chart.ui.chart.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;


import javax.inject.Inject;
import javax.inject.Named;

import inc.minuth.mi_chart.R;
import inc.minuth.mi_chart.ui.chart.ChartView;
import inc.minuth.mi_chart.ui.chart.adapter.viewholder.ChatViewHolder;
import inc.minuth.mi_chart.data.firebase.model.Chat;

public class ChatAdapter
{
    private FirebaseRecyclerAdapter adapter;
    private ChatViewHolder holder ;
    private DatabaseReference query;
    @Inject
    public ChatAdapter(@Named("chats")DatabaseReference query)
    {
        this.query = query;
    }
    public void attatch(final ChartView chartView)
    {
        final LayoutInflater inflater = LayoutInflater.from(chartView.geContext());
        FirebaseRecyclerOptions<Chat> options =
                new FirebaseRecyclerOptions.Builder<Chat>()
                        .setQuery(query, Chat.class)
                        .build();
        adapter = new FirebaseRecyclerAdapter<Chat, ChatViewHolder>(options) {

            @NonNull
            @Override
            public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view=inflater.inflate(R.layout.layout_message,null);
                holder = new ChatViewHolder(view);
                return holder;
            }

            @Override
            protected void onBindViewHolder(@NonNull ChatViewHolder holder, int position, @NonNull Chat model) {

                holder.bindView(model);
            }

            @Override
            public void onDataChanged() {
                chartView.onDataChange();
            }
        };
    }

    public FirebaseRecyclerAdapter getAdapter()
    {
        return adapter;
    }
    public void start()
    {
        adapter.startListening();
    }
    public void stop()
    {
        adapter.stopListening();
    }
}
