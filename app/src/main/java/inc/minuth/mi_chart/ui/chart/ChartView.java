package inc.minuth.mi_chart.ui.chart;

import android.app.Activity;
import android.content.Context;

public interface ChartView
{
    void onDataChange();
    void refreshMember(long count);
    Context geContext();

}
