package inc.minuth.mi_chart.data.firebase.repository;

import android.support.annotation.NonNull;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import javax.inject.Inject;
import javax.inject.Named;

import inc.minuth.mi_chart.callback.DataListenter;
import inc.minuth.mi_chart.data.firebase.model.User;

public class UserRepository
{
    private DatabaseReference reference;
    private long count;
    private FirebaseAuth firebaseAuth;
    @Inject
    public UserRepository(@Named("users")DatabaseReference reference,FirebaseAuth firebaseAuth)
    {
        this.reference = reference;
        this.firebaseAuth = firebaseAuth;

    }
    public void addDataListenter(final DataListenter<Long> dataListenter)
    {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                count = dataSnapshot.getChildrenCount();
                dataListenter.onDataChanged(count);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void addUser(User user)
    {
        reference.push().setValue(user);
    }

    public FirebaseUser getCurrentUser()
    {
        return firebaseAuth.getCurrentUser();
    }

}
