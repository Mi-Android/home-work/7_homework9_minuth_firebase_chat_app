package inc.minuth.mi_chart.data.firebase.repository;

import com.google.firebase.database.DatabaseReference;

import javax.inject.Inject;
import javax.inject.Named;

import inc.minuth.mi_chart.data.firebase.model.Chat;

public class ChatRepository
{
    DatabaseReference reference;
    @Inject
    public ChatRepository(@Named("chats")DatabaseReference reference)
    {
        this.reference =  reference;
    }
    public void sendMessageChat(Chat chat)
    {
        reference.push().setValue(chat);
    }

}
