package inc.minuth.mi_chart.utils;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import inc.minuth.mi_chart.data.firebase.model.User;

public class UserUtils
{

    private static FirebaseUser currentUser;
    private static FirebaseUser getCurrentUser()
    {
        if(currentUser == null)
        {
            currentUser = FirebaseAuth.getInstance().getCurrentUser();
        }
        return currentUser;
    }
    public static boolean isMe(User user)
    {
        return user.getId().equals(getCurrentUser().getUid());
    }
    public static String getMyId()
    {
        return getCurrentUser().getUid();
    }
    public static String getUserName(){return getCurrentUser().getDisplayName();}
    public static String getEmail(){return  getCurrentUser().getEmail();}

}
