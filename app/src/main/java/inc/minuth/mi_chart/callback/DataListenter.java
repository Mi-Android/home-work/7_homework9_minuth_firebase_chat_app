package inc.minuth.mi_chart.callback;

public interface DataListenter<T>
{
    void onDataChanged(T parameter);
}
