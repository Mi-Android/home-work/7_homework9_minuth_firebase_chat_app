package inc.minuth.mi_chart.di.component;

import javax.inject.Scope;
import javax.inject.Singleton;

import dagger.Component;
import inc.minuth.mi_chart.di.module.FirebaseModule;
import inc.minuth.mi_chart.ui.chart.ChartFragment;
import inc.minuth.mi_chart.ui.main.MainActivity;
@Singleton
@Component(modules = {FirebaseModule.class})
public interface AppComponent
{
    void inject(MainActivity activity);
    void inject(ChartFragment fragment);
}
